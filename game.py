# homework 1
import random

lists = []
list_pow = 3
level = 1
win_count = 3
win_ = []


def generate_list(n):
    global win_
    global lists
    lists.clear()
    p = n * n
    main_ = 0
    auxiliary_ = n-1
    horizon, vertical, main, auxiliary = [], [], [], []
    for i in range(p):
        lists.append(' ')
        horizon.append(i)
        element = i + 1
        if element % n == 0:  # horizon
            win_.append(horizon.copy())
            horizon.clear()

        if len(vertical) <= i % n:  # vertical
            vertical.append([])
        vertical[i % n].append(i)

        if main_ == i:  # main
            main.append(i)
            main_ += n+1

        if auxiliary_ == i and i < p-1:  # auxiliary
            auxiliary.append(i)
            auxiliary_ += n-1

    win_.extend(vertical)
    win_.append(main)
    win_.append(auxiliary)

# win_ = [
#     [0, 1, 2], [3, 4, 5], [6, 7, 8],
#     [0, 3, 6], [1, 4, 7], [2, 5, 8],
#     [0, 4, 8], [2, 4, 6]
# ]

def print_x_o(lists):
    print("\n")
    p = list_pow*list_pow
    print('______'*list_pow, sep="")
    for i in range(len(lists)):
        if 0 < i and int(i+1) % list_pow == 0:
            print("|     "*list_pow, '|', sep="")
            for j in range(i-list_pow+1, i+1):
                print(f"|  {lists[j]}  ", end="")
                if j == i:
                    print("|")
            if p - 1 != i:
                print("|_____"*list_pow, "|", sep="")

    print('|_____'*list_pow,"|", sep="")

    print("\n")


def computer():
    check = True
    if level > 1:
        check = computer_check("O")
        if check and level > 2:
            check = computer_check("X")

    while check:
        o = random.randint(0, list_pow*list_pow-1)
        if lists[o] == " ":
            lists[o] = "O"
            check = False


def computer_check(string: str) -> bool:
    check = True
    max_k, index_ = 0, 0
    for i in win_:
        k = 0
        index = None
        for j in i:
            if lists[j] == string:
                k += 1
            elif index is None and lists[j] == " ":
                index = j

        if k > max_k:
            max_k = k
            index_ = index

        if k == win_count-1:
            lists[index] = "O"
            check = False
            break

        # elif string == "X" and max_k > 0:
        #     lists[index_] = "O"
        #     check = False
        #     break

    return check


def win(string: str) -> bool:
    for i in win_:
        check = False
        k = 0
        for j in i:
            if lists[j] == string:
                k += 1
                if win_count == k:
                    check = True
            else:
                k = 0
        if check:
            return check


def level_func():
    global level
    while True:
        try:
            level = int(input("tell me level: LOW: 1, MIDDLE: 2, HIGH: 3 \n"))
        except ValueError:
            print("Wrong Tell me numbers 1 or 2 or 3 !!! Try Again: ")
            continue

        if level < 1 or level > 3:
            print("Wrong Tell me numbers 1 or 2 or 3 !!! Try Again: ")
            continue

        break


def list_func():
    global list_pow
    global win_count
    while True:
        try:
            list_pow = int(input("tell me more than 3 digits \n"))
        except ValueError:
            print("enter more than 3 digits !!! Try Again: ")
            continue

        if list_pow < 3:
            print("enter more than 3 digits !!! Try Again: ")
            continue

        break

    if list_pow <= 5:
        win_count = list_pow
    else:
        win_count = 5

    generate_list(list_pow)


def play():
    list_func()
    level_func()

    p = list_pow * list_pow
    while True:
        try:
            x = int(input(f"Tell one of the numbers 1 through {p}: "))
        except ValueError:
            print(f"Wrong Tell me numbers 1 through {p} !!! Try Again: ")
            continue

        if x < 1 or x > p:
            print(f"Wrong Tell me numbers 1 through {p} !!! Try Again: ")
            continue

        if lists[x - 1] != " ":
            print(f"Wrong Tell me numbers 1 through {p} !!! Try Again: ")
            continue

        lists[x - 1] = "X"
        if win("X"):
            print_x_o(lists)
            print("You won!! 😊 👍👍")
            play_again()
            break

        if " " not in lists:
            print_x_o(lists)
            print("draw 😏 👌👌")
            play_again()
            break

        computer()
        print_x_o(lists)

        if win("O"):
            print("you lose 😞 👎👎")
            play_again()
            break


def play_again():
    check = input("Play Again Yes or No \n")
    if check == "Yes":
        play()
    else:
        print("Good By")

play()
