# homework 2

sample_list = [[10, 20], [40], [30, 56, 25], [10, 20], [33], [40]]
new_list = []

for i in sample_list:
    if i not in new_list:
        new_list.append(i)

print(new_list)

# homework 3

sample_list_1 = [0, 10, [20, 30], 40, 50, [60, 70, 80], [90, 100, 110, 120]]
new_list_1 = []

for i in sample_list_1:
    if type(i) == list:
        for j in i:
            new_list_1.append(j)
    else:
        new_list_1.append(i)
print(new_list_1)

# homework 4

sample_list_2 = [1, 1, 2, 3, 4, 4, 5, 1]
new_list_2 = [[], []]

first_part = int(input("Length of the first part of the list: "))
for index, value in enumerate(sample_list_2):
    if index < first_part:
        new_list_2[0].append(value)
    else:
        new_list_2[1].append(value)
print(new_list_2)
